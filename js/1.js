$(document).ready(function(){

		var lineChartData = {
			labels : ["January","February","March","April","May","June","July"],
			datasets : [
				{
					fillColor : "rgba(66,152,215,0.5)",
					strokeColor : "rgba(66,152,215,1)",
					pointColor : "rgba(66,152,215,1)",
					pointStrokeColor : "#fff",
					data : [28,38,40,45,80,97,99]
				}
			]
			
		}

			var lineChartData1 = {
			labels : ["January","February","March"],
			datasets : [
				{
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					data : [65,59,90]
				},
				{
					fillColor : "rgba(66,152,215,0.5)",
					strokeColor : "rgba(66,152,215,1)",
					pointColor : "rgba(66,152,215,1)",
					pointStrokeColor : "#fff",
					data : [28,48,60]
				}
			]
			
		}

	var myLine = new Chart(document.getElementById("myChart").getContext("2d")).Line(lineChartData);
	var myLine = new Chart(document.getElementById("myChart02").getContext("2d")).Line(lineChartData1);


 });